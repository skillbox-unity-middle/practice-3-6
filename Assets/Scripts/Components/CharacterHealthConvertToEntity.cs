using Unity.Entities;
using UnityEngine;

public class CharacterHealthConvertToEntity : MonoBehaviour, IConvertGameObjectToEntity
{
    public int health = 100;

    private EntityManager _dstManager;
    private Entity _entity;

    public void SetCharacterHealthEntity(int hp)
    {
        health += hp;
        _dstManager.SetComponentData(_entity, new CharacterHealthData
        {
            health = health,
        });
    }

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        dstManager.AddComponentData(entity, new CharacterHealthData
        {
            health = health
        });

        _entity = entity;
        _dstManager = dstManager;
    }
}
